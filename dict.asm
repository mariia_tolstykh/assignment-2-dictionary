%include "lib.inc"

global find_word

section .text

;rdi - указатель на строку, rsi - указатель на начало словаря
find_word: 
    .loop:
        test rsi, rsi       ;проверка на конец словаря
        jz .no    
        push rdi
	push rsi
        add rsi, 8          
        call string_equals  ;1 если равны строки, 0 если нет
        pop rsi
	pop rdi
        test rax, rax       
        jnz .yes          ;если не 0, то строки равны
        mov rsi, [rsi]      ;след слово в словаре
        jmp .loop
    .yes:
        mov rax, rsi       
        ret
    .no:
        xor rax, rax       
        ret

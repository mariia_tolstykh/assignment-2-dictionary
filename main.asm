%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define size_buffer 256

global _start

section .rodata
read_error: db "input error", 0
not: db "key not found", 0

section .bss
buffer: resb 256		;выделяем буфер

section .text

_start:		
	mov rdi, buffer		
	mov rsi, size_buffer		
	call read_word 		;адрес буфера в rax, длину слова в rdx, иначе 0 в rax
	test rax, rax 
	jz .read_error 	;если 0, то не прочитается
	mov rdi, buffer 		
	mov rsi, next 		
	call find_word		
	test rax, rax
	jz .not		;если 0, то слово не найдено 
	mov rdi, rax       
	add rdi, 8         
	call string_length 
	add rdi, rax       
	inc rdi            
	jmp .end
	.read_error:
		mov rdi, read_error      ;ошибка ввода
		jmp .end

	.not:
		mov rdi, not		;ошибка не найден ключ 
		
	.end:
		call print_string    
		xor rdi, rdi
		call exit

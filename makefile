PYC = python3
ASC = nasm
ASM_FLAGS = -felf64

%.o: %.asm	
	@$(ASC) $(ASM_FLAGS) -o $@ $<

program: main.o lib.o dict.o
	@ld -o $@ $^

clean: 
	@rm *.o
test:
	@$(PYC) test.py

.PHONY: clean test
